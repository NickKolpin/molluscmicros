PCBNEW-LibModule-V1  Tue 01 Sep 2015 14:33:52 EDT
# encoding utf-8
Units mm
$INDEX
microusb-b
$EndINDEX
$MODULE microusb-b
Po 0 0 0 15 55E5EF80 00000000 ~~
Li microusb-b
Sc 0
AR 
Op 0 0 0
T0 0 0 1 1 0 0.15 N V 21 N "microusb-b"
T1 0 0 1 1 0 0.15 N V 21 N "VAL**"
DS 3.81 -1.27 3.81 3.81 0.15 21
DS 3.81 3.81 -3.81 3.81 0.15 21
DS -3.81 3.81 -3.81 -3.81 0.15 21
DS -3.81 -3.81 3.81 -3.81 0.15 21
DS 3.81 -3.81 3.81 -1.27 0.15 21
$PAD
Sh "3" R 0.4 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -2.85
$EndPAD
$PAD
Sh "4" R 0.4 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.65 -2.85
$EndPAD
$PAD
Sh "5" R 0.4 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.3 -2.85
$EndPAD
$PAD
Sh "2" R 0.4 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.65 -2.85
$EndPAD
$PAD
Sh "1" R 0.4 1.4 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.3 -2.85
$EndPAD
$PAD
Sh "6" R 2.3 1.9 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.75 0
$EndPAD
$PAD
Sh "7" R 2.3 1.9 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.75 0
$EndPAD
$EndMODULE microusb-b
$EndLIBRARY
