PCBNEW-LibModule-V1  Mon 31 Aug 2015 16:25:31 EDT
# encoding utf-8
Units mm
$INDEX
xtal
$EndINDEX
$MODULE xtal
Po 0 0 0 15 55E4B829 00000000 ~~
Li xtal
Sc 0
AR 
Op 0 0 0
T0 0 0 1 1 0 0.15 N V 21 N "xtal"
T1 0 0 1 1 0 0.15 N V 21 N "VAL**"
$PAD
Sh "1" R 5 1.9002 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.5 0
$EndPAD
$PAD
Sh "2" R 5 1.9002 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.5 0
$EndPAD
$EndMODULE xtal
$EndLIBRARY
